import { Component } from '@angular/core';

@Component({
  selector: 'app-root, wsb-root, wsb[lubie=angular]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular WSB';
}
