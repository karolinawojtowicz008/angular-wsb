import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private oAuth: OAuthService) {
    this.oAuth.configure(environment.authConfig)
  }

  async init() {
    await this.oAuth.tryLogin({})
    const token = this.getToken()
    console.log(token)
    if(!token){
      this.login()
    }
  }

  getToken() {
    return this.oAuth.getAccessToken();
  }

  login() {
    this.oAuth.initLoginFlow()
  }

  logout() {
    this.oAuth.logOut()
  }
}
