import { ErrorHandler, Injectable } from '@angular/core';
import { Album, AlbumsSearchResponse, SimpleAlbum } from '../model/Search';
// import { MusicSearchModule } from 'src/app/music-search/music-search.module';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from './auth.service';

const mockAlbums: SimpleAlbum[] = [
  { id: '123', name: 'Album from service 123', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }] },
  { id: '234', name: 'Album from service 234', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/200' }] },
  { id: '345', name: 'Album from service 345', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }] },
  { id: '456', name: 'Album from service 456', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }] },
]

@Injectable({
  // providedIn: MusicSearchModule
  providedIn: 'root'
})
export class MusicSearchService {

  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private errorHandler: ErrorHandler
  ) {
    // console.log('MusicSearchService Created')
  }

  search(query: string) {

    return this.http.get<AlbumsSearchResponse>('https://api.spotify.com/v1/search', {
      headers: { Authorization: 'Bearer ' + this.auth.getToken() },
      params: { type: 'album', q: query }
    }).pipe(
      // pluck('albums','items')
      // map(res => (res.albums as any).dawd.ad.aw.d.items),
      map(res => res.albums.items),
      catchError(error => {
        this.errorHandler.handleError(error)

        if (!(error instanceof HttpErrorResponse)) {
          return throwError(new Error('Unexpected error'))
        }

        if (!isSpotifyError(error.error)) {
          return throwError(new Error('Unexpected server error'))
        }

        if (error.status === 401) {
          setTimeout(() => this.auth.login(), 1500)
        }
        
        return throwError(new Error(error.error.error.message))
        // return of(mockAlbums as Album[])
      })
    )

  }
}

import { catchError, map, pluck } from 'rxjs/operators'
import { of, throwError } from 'rxjs';

interface SpotifyError {
  error: {
    message: string
  }
}
function isSpotifyError(someError: any): someError is SpotifyError {
  return Boolean(someError?.error?.message)
}