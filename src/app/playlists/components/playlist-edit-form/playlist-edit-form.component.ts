import { AfterViewInit, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/core/model/Playlist';

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss']
})
export class PlaylistEditFormComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() playlist!: Playlist

  draft!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  @ViewChild('nameRef')
  nameRef?: ElementRef<HTMLInputElement>;

  constructor() {
    console.log('constructor')
  }

  submit() {
    this.save.emit(this.draft) // this.draft => $event
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    // debugger
    console.log('ngOnChanges')
    this.draft = { ...this.playlist }
  }

  ngOnInit(): void {
    // this.draft = Object.assign({},this.playlist)
    // this.draft = { ...this.playlist /* ,tracks:[...this.playlist.tracks] */ }
    console.log('ngOnInit')
    if (!this.playlist) {
      throw new Error('Playlist is required!')
    }
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    console.log('ngDoCheck - check and rerender')
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    console.log('ngOnDestroy!')
  }

  cancelClick() {
    this.cancel.emit()
  }


  ngAfterViewInit(): void {
    console.log('ngAfterViewInit')
    this.nameRef?.nativeElement.focus()
  }

  reset() {
    this.playlist = {
      id: '',
      name: '',
      public: false,
      description: ''
    }
  }

}
